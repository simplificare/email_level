# Email Level Plugin for Direct Admin

A modified version of the Email Level Plugin for Direct Admin.

## Installation Instructions
1. Log in to Direct Admin Control Panel at Admin Level.
2. Goto Extra Features -> Plugin Manager.
3. Click the ADD button.
4. Select URL method, enter https://www.simplificare.net/da/email_level.tar.gz, enter your admin password, check Install after upload then click install.

## Changelog

### Chagnes in v2.0.2
- Updated remote calls to DirectAdmin to use API commands instead of browser commands.
- Updated copyright year.

### Changes in v2.0.1
- Allowed [] in passwords
- Updated iOS automatic configuration profile name to better identify what account the profile is connected to

### Changes in v2.0.0
- Added installation instructions to README.
- Added automatic configuration profiles for iOS/iPadOS and MacOS Mail (10.8+) (profiles are not signed)
- Dropped "s" from the version number

### Changes in v1.9s
- Forced https loading on login script
- Added and re-themed the pages using Bootstrap
- Fixed a bug loading morning/afternoon/evening on vacation page when a message exists

### Changes in v1.8s
- Updated the Roundcube Logo
- Fixed the day and month bug in vacation responders
- Made the year more dynamic (based on server date, last year and +5 years)  in vacation responders
- Hid "http" at the user level install screen, because of this: forced use of SSL in the user config.php include
- Updated the readable_byte function to output a little cleaner
- Default disabled SquirrelMail and Uebimiau webmail in the user interface via user config.php include

## Known Issues
- User level installer does not indicate that the files were successfully installed.

Feel free to fork/modify as you see fit, credit to JBMC Software for initial script.
