<?php

require_once("login.php");
require("payload.php");

if ( @$_POST['process'] == 1 ) {
	
	$description = $_SESSION['login']; //$_POST["description"];
	$email = $_SESSION['login']; //$_POST["email"];
	$fullname = $_POST["displayName"];
	$username = $_SESSION['login']; //$_POST["username"];
	$org = $_SESSION['domain']; //'SomeOrg';
	$file_name = $username . '_profile.mobileconfig';
	$server_in = 'mail.' . $_SESSION['domain']; //"some.server.com";
	$server_out = $server_in;
	$file_name = $username . '_mail.mobileconfig';
	
	// Add headers to set content type to xml and download on submit
	
	header("Content-Type: application/xml; charset=utf-8");
	header("Content-Transfer-Encoding: Binary");
	header("Content-disposition: Attachment; filename=\"" . $file_name . "\"");
	
	$payload = new Payload($org);
	$identifier = $payload->getIdentifier();
	$mail = new Mail($description, $fullname, $email, $username, $identifier, $server_in, $server_out, $org);
	$payload->addPayloadContent($mail->getXML());
	$xml = $payload->getXML()
	?>
	<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
	<plist version="1.0">
	<dict>
	<?=$xml?>
	</dict>
	</plist>

<?php	
} else {
	die('Request must be submitted by form.');
}
?>