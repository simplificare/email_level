<?php
require_once("login.php");

$startstamp=time();
$endstamp=time();
$text="";


$time_of_day = array( "morning" => "Morning", "afternoon" => "Afternoon", "evening" => "Evening");
$months = array();
for ($i=1; $i<=12; $i++)
{
	$months[$i] = date("M", mktime(0,0,0,$i,1,0));
}
$days = array();
for ($i=1; $i<=31; $i++)
{
	$days[$i] = date("d", mktime(0,0,0,1,$i,0));
}

$start_year = date("Y", strtotime("last year"));
$end_year = date("Y", strtotime("+5 years"));
$years = array();
for ($i=$start_year; $i<=$end_year; $i++)
{
        $years[$i] = $i;
}

$custom_reply_headers = 0;
$reply_subject='';
$reply_charset = '';
$reply_encodings='';
$reply_content_types='';
$reply_once_select='';


if (isset($_POST['action']) && ($_POST['action'] == "create" || $_POST['action'] == "modify" || $_POST['action'] == "delete"))
{
	include("include/header.php");

	$post = array(
				'user' => $_SESSION['user'],
				'domain'=>$_SESSION['domain'],
				'password' => $_SESSION['password'],
				'action' => $_POST['action'],
				'text' => stripslashes($_POST['text']),
				'starttime' => $_POST['starttime'],
				'startmonth' => date("m", mktime(0,0,0,$_POST['startmonth'],1,0)),
				'startday' => $_POST['startday'],
				'startyear' => $_POST['startyear'],
				'endtime' => $_POST['endtime'],
				'endmonth' => date("m", mktime(0,0,0,$_POST['endmonth'],1,0)),
				'endday' => $_POST['endday'],
				'endyear' => $_POST['endyear']
			);
			
	if (isset($_POST['custom_reply_headers']) && $_POST['custom_reply_headers']==1)
	{
		$post['subject'] = $_POST['subject'];
		$post['reply_encoding'] = $_POST['reply_encoding'];
		$post['reply_content_type'] = $_POST['reply_content_type'];
		$post['reply_once_time'] = $_POST['reply_once_time'];
	}


	$sock = newSock();
	$sock->query('/CMD_API_EMAIL_ACCOUNT_VACATION', $post);

	$result = $sock->fetch_parsed_body();

	if ( $result['error'] != "0" )
	{
        	no_go("Unable to set vacation message:<br><b>".$result['text']."</b>");
	}

	echo "Vacation message ";
	switch ($_POST['action'])
	{	case "create" : echo "created"; break;
		case "modify" : echo "updated"; break;
		case "delete" : echo "deleted"; break;
	}
	echo ".<br><br><a href='index.php'>Click here</a> to go back.";

	include("include/footer.php");
	exit(0);
}

//grab any vacation message if there is one.
$sock1 = newSock();
$sock1->query('/CMD_EMAIL_ACCOUNT_VACATION',
        array(
                'user' => $_SESSION['user'],
                'domain'=>$_SESSION['domain'],
                'password' => $_SESSION['password'],
        ));

$result1 = $sock1->fetch_parsed_body();

$exists=0;

if ($result1 == 0)
{
        no_go("socket retuned a zero result");
}



/*
echo "<textarea cols=120 rows=20>";
//echo $sock1->fetch_body();
echo "\n\n";
print_r($result1);
echo "</textarea><br>\n";
*/

if ( $result1['error'] == "0" )
{
        if (!isset($result1['startyear']))
        {
    		include("include/header.php");
            no_go("Socket returned no error, but there is data missing. Try reloading this page.<br/><br/>Or <a href=\"vacation.php\">Click here</a> to try again.");
        }

        $startstamp=getTimeFromVars($result1['startyear'], $result1['startmonth'], $result1['startday'], $result1['starttime']);
        $endstamp=getTimeFromVars($result1['endyear'], $result1['endmonth'], $result1['endday'], $result1['endtime']);
        $text=$result1['text'];
        $exists=1;

		load_header_variables($result1);
}
else
{
        //echo, I guess it doesn't exist yet.
        load_header_variables($result1);
}

include("include/header.php");

?>
<!--Set vacation message container-->
	<div class="container">
		<div class="row">
			<div class="col">
				<h4>Set Vacation Message</h4>
				<p>Complete the form below to set a vacation message.</p>
			</div> <!-- /col -->
		</div> <!-- /row -->
		
		<form action="?" method="POST">
			<input type=hidden name="action" value="<?php if ($exists) { echo "modify";}else{echo "create";}?>">
			<?php
				if ($custom_reply_headers) {
					echo '  <input type="hidden" name="custom_reply_headers" value="1">';
					echo '	<div class="form-group row">
								<label for="subject" class="col-sm-2 col-form-label">Subject Prefix:</label>
								<div class="input-group col-sm-6">
									<input type="text" class="form-control" name="subject" value="'.$reply_subject.'">
									<div class="input-group-append">
										<span class="input-group-text">: Original Subject</span>
							        </div>
								</div>
							</div>';
					echo '  <div class="form-group row">
								<label for="reply_encoding" class="col-sm-2 col-form-label">Reply Encoding:</label>
								<div class="col-sm-6">
									'.$reply_encodings.' 
									<!-- <small class="form-text text-muted">(browser: '.$reply_charset.')</small> -->
						 ';
									if (!$exists) {
										echo '<small class="form-text text-muted">Note: set and save this option first so you don\'t loose your changes.</small>';
									}
					echo '		</div>
							</div>
						 ';
					echo '	<div class="form-group row">
								<label for="reply_content_type" class="col-sm-2 col-form-label">Content-Type:</label>
								<div class="col-sm-6">
									'.$reply_content_types.'
								</div>
							</div>
						 ';	 
					echo '	<div class="form-group row">
								<label for="reply_once_time" class="col-sm-2 col-form-label">Reply Frequency:</label>
								<div class="col-sm-6">
									'.$reply_once_select.'
									<small class="form-text text-muted">Minimum time before a repeated reply</small>
								</div>
							</div>
						 ';	 
				}
			?>
			<div class="form-group row">
				<label for="text" class="col-sm-2 col-form-label">Vacation Message:</label>
				<div class="col-sm-6">
					<textarea class="form-control" id="vacationMessageTextArea" rows="5" name="text"><?php echo $text;?></textarea>
				</div>
			</div>
		
			<div class="form-group row">
				<label for="text" class="col-sm-2 col-form-label">Start Date:</label>
				<div class="col-sm-6">
					<div class="form-group">
		                <div class="input-group date" id="startdategroup" data-target-input="nearest">
							<!--
		                    <select name="starttime" class="custom-select" id="inputGroupSelect">
								<option value="morning">Morning</option>
								<option value="afternoon">Afternoon</option>
								<option value="evening">Evening</option>
							</select>
							<div class="input-group-addon">
								<span class="input-group-text">of</span>
							</div>
		                    <input type="text" class="form-control datetimepicker-input" id="startdate" data-toggle="datetimepicker" data-target="#startdate"/>
		                    <div class="input-group-append" data-target="#startdate" data-toggle="datetimepicker">
		                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
		                    </div>
							-->
							<?php showTime("start", $startstamp); ?>
		                </div>
		            </div>
				</div>
			</div>
			
			<div class="form-group row">
				<label for="text" class="col-sm-2 col-form-label">End Date:</label>
				<div class="col-sm-6">
					<div class="form-group">
		                <div class="input-group date" id="enddategroup" data-target-input="nearest">
		                    <!--<select name="endtime" class="custom-select" id="inputGroupSelect">
								<option value="morning">Morning</option>
								<option value="afternoon">Afternoon</option>
								<option value="evening">Evening</option>
							</select>
							<div class="input-group-addon">
								<span class="input-group-text">of</span>
							</div>
		                    <input type="text" class="form-control datetimepicker-input" id="enddate" data-toggle="datetimepicker" data-target="#enddate"/>
		                    <div class="input-group-append" data-target="#enddate" data-toggle="datetimepicker">
		                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
		                    </div>-->
		                    <?php showTime("end", $endstamp); ?>
		                </div>
		            </div>
				</div>
			</div>
			
			<div class="form-group row">
				<label for="text" class="col-sm-2 col-form-label">Current Server Time:</label>
				<div class="col-sm-6">
					<?php echo get_tod(time())." of ".date("M j, Y"); ?>
				</div>
			</div>
			
			<div class="form-group row">
				<div class="col-sm-6">
					<input type="submit" value="<?php if($exists){echo "Update";}else{echo "Set";}?> Vacation Message" class="btn btn-dark">
				</div>
			</div>
					
		</form>
		
		<?php if ($exists) { ?>
			<form action="?" method="POST">
				<input type="hidden" name="action" value="delete">
				<input type="submit" value="Delete Current Vacation Message" class="btn btn-danger">
			</form>
		<?php } ?>
		
	</div> <!-- /set vacation message container-->
	
	<!-- /add bootstrap class to select fields-->
	<script type="text/javascript">
	$( document ).ready(function() {
	    $(".selectclass").addClass("form-control");
	    $("#startdategroup select").addClass("form-control custom-select");
	    $("#enddategroup select").addClass("form-control custom-select");
		/*
	    $('#startdate').datetimepicker({
            format: 'L',
        });
        $('#enddate').datetimepicker({
            format: 'L',
        });
		*/
	});
	</script>

<?php

include("include/footer.php");

function show_select($name, $arr, $selected='') {
	echo "<select name='$name'>";
	foreach($arr as $v => $t) {
		if ($v == $selected) {
			echo "\t<option selected value='$v'>$t</option>\n";
		} else {
			echo "\t<option value='$v'>$t</option>\n";
		}
	}
	echo "</select>\n";

}

function get_tod($stamp) {
	$hour = (int)date("H", $stamp);
	if (0 <= $hour && $hour < 12) { return "morning"; }
	if (12 <= $hour && $hour < 18) { return "afternoon"; }
	return "evening";
}

function getTimeFromVars($year, $month, $day, $tod) {
	switch($tod) {
		case "morning" : $hour = 6; break;
		case "afternoon" : $hour = 12; break;
		case "evening" : $hour = 18; break;
		default : $hour = 0; break;
	}
	return mktime($hour, 0, 0, $month, $day, $year);
}

function showTime($prefix, $stamp=0) {
	global $time_of_day, $months, $days, $years;

	show_select("${prefix}time", $time_of_day, get_tod($stamp));

	show_select("${prefix}month", $months, date("m", $stamp));

	show_select("${prefix}day", $days, date("j", $stamp));

	show_select("${prefix}year", $years, date("Y", $stamp));


}

function load_header_variables($result1) {
	global $custom_reply_headers, $reply_subject, $reply_charset, $reply_encodings, $reply_content_types, $reply_once_select;

	if (!isset($result1['custom_reply_headers'])) return;
	if ($result1['custom_reply_headers'] != '1') return;

	$custom_reply_headers = 1;

	$reply_subject=$result1['reply_subject'];
	$reply_charset=$result1['reply_charset'];
	$reply_encodings=$result1['reply_encodings'];
	$reply_content_types=$result1['reply_content_types'];
	$reply_once_select=$result1['reply_once_select'];

	if ($reply_charset == '') {
		//$reply_charset = 'iso-8859-1';
		$reply_charset = 'UTF-8';
	}

	header("Content-Type: text/html; charset=$reply_charset");
}

?>
