<?php
require_once("login.php");
include("include/header.php");
?>

	<!-- usage container -->
	<?php
		// do the math first
		$quota = $_SESSION['quota'];
		$total = $_SESSION['total'];
		$inbox = $_SESSION['inbox'];
		$spam = $_SESSION['spam'];
		$folders = $_SESSION['imap'];
		
		if ($quota != 0) {
			$percentage = round($total / $quota * 100,1);		
			
			if ($percentage <= 50) {
				$barClass = "bg-success";
			} elseif ($percentage > 50 && $percentage <= 80) {
				$barClass = "bg-warning";
			} elseif ($percentage > 80 && $percentage <= 100) {
				$barClass = "bg-danger";
			}
		}
	?>	
	<div class="container">
		<div class="row">
			<div class="col">
				<h4>Mailbox Usage</h4>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<?php
					if ($quota != 0) {
				?>
					<p>Your quota is <?php echo readable_byte($quota); ?>. You are currently using <?php echo readable_byte($total); ?> (<?php echo $percentage;?>%) of your mail storage.</p>
					<div class="progress">
						<div class="progress-bar <?php echo $barClass;?>" role="progressbar" style="width: <?php echo $percentage; ?>%" aria-valuenow="<?php echo $percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				<?php
					} else {
				?>
					<p>Your quota is unlimited. You are currently using <?php echo readable_byte($total); ?> of mail storage.</p>
				<?php
				}	
				?>
			</div>
		</div>
	</div>
	<!-- /end automatic settings container
	
	<!--Access your Webmail Container-->
	<div class="container mb-4">
		<div class="row">
			<div class="col">
				<h4>Access Your Email</h4>
				<p>Open the webmail application in a new window by clicking the link below.</p>
			</div>
		</div>
		<!-- card holder for mail providers -->
		<div class="card-deck text-center">
		    <?php showLinkIfExists("/roundcube", "Roundcube", "roundcube_logo.png", $show_roundcube); ?>
			<?php showLinkIfExists("/squirrelmail", "SquirrelMail", "squirrel.gif", $show_squirrelmail); ?>
			<?php showLinkIfExists("/webmail", "Uebimiau", "webmail.gif", $show_uebimiau); ?>
		</div> <!-- end card-deck -->
	</div>
	<!--end access your webmail container-->

	<!-- automatic settings container -->	
	<div class="container mb-4">
		<div class="row">
			<div class="col">
				<h4>Automatic Mail Configuration Profiles</h4>
				<p>Use the button below to download an automatic configuration profile for your mail application. If the application you are looking for is not listed, you can use the manual settings below.</p>
				<?php
				if (version_compare(phpversion(), '7.0.0', '>')) { //current version of PHP must be > 7.0.0 to show the iOS button.
				?>
				    <!-- iOS Button modal -->
					<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#iosModal">
						iOS and MacOS (10.8+) Mail.app
					</button>
				<?php 
				} else {
				?>
					<span class="d-inline-block" data-toggle="popover" data-trigger="hover" data-content="You must use PHP 7 or greater to download this configuration profile. Current version is <?php echo phpversion(); ?>.">
						<button type="button" class="btn btn-dark" style="pointer-events: none;" disabled data-toggle="modal" data-target="#iosModal">
							iOS and MacOS (10.8+) Mail.app
						</button>
					</span>
				<?php 
				} 
				?>
			</div>
		</div>
	</div>
	<!-- /end automatic settings container
	
	<!-- manual settings container -->
	<div class="container mb-4">
		<div class="row">
			<div class="col">
				<h4>Manual Mail Client Settings</h4>
				<p>Use the settings below to configure your email client manually.</p>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div id="ssl" class="card">
					<div class="card-header bg-primary text-white">
						Secure <abbr title="Secure Sockets Layer">SSL</abbr>/<abbr title="Transport Layer Security">TLS</abbr> Settings (Recommended)
					</div>
					<table class="table">
						<tbody>
							<tr>
								<td>Username:</td>
								<td class="data wrap-text"><?php echo $_SESSION['user']; ?>@<?php echo $_SESSION['domain']; ?></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td class="escape-note"> Use the email account’s password.</td>
							</tr>
							<tr>
								<td>Incoming Server:</td>
								<td class="data">mail.<?php echo $_SESSION['domain']; ?>
									<ul class="list-inline">
										<li><abbr title="Internet Message Access Protocol" class="initialism">IMAP</abbr> Port: 993</li>
										<li><abbr title="Post Office Protocol 3" class="initialism">POP3</abbr> Port: 995</li>  
									</ul>
								</td>
							</tr>
							<tr>
								<td>Outgoing Server:</td>
								<td class="data">mail.<?php echo $_SESSION['domain']; ?>
									<ul class="port_list list-inline">
										<li><abbr title="Simple Mail Transfer Protocol">SMTP</abbr> Port: 465</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="notes">
									<div class="small_note">IMAP, POP3, and SMTP require authentication.</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div> <!-- ssl card definition -->
			</div> <!-- /ssl column definition -->
			
			<div class="col">
				<div id="nonssl" class="card">
					<div class="card-header bg-warning text-white">
						Non-SSL Settings (NOT Recommended)
					</div>
					<table class="table">
						<tbody>
							<tr>
								<td>Username:</td>
								<td class="data wrap-text"><?php echo $_SESSION['user']; ?>@<?php echo $_SESSION['domain']; ?></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td class="escape-note"> Use the email account’s password.</td>
							</tr>
							<tr>
								<td>Incoming Server:</td>
								<td class="data">mail.<?php echo $_SESSION['domain']; ?>
									<ul class="list-inline">
										<li><abbr title="Internet Message Access Protocol" class="initialism">IMAP</abbr> Port: 143</li>
										<li><abbr title="Post Office Protocol 3" class="initialism">POP3</abbr> Port: 110</li>  
									</ul>
								</td>
							</tr>
							<tr>
								<td>Outgoing Server:</td>
								<td class="data">mail.<?php echo $_SESSION['domain']; ?>
									<ul class="list-inline">
										<li><abbr title="Simple Mail Transfer Protocol">SMTP</abbr> Port: 587</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="notes">
									<div class="">IMAP, POP3, and SMTP require authentication.</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div> <!-- non ssl card definition -->
			</div> <!-- /non ssl column definition -->
		</div><!-- /settings cards row -->
	</div>
	<!-- end email settings container -->	
	
	<!-- Modal for iOS Automatic Configuration Profile -->
	<div class="modal fade" id="iosModal" tabindex="-1" role="dialog" aria-labelledby="iosModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="iosModalLabel">Please complete the following</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="payloadinput_submit.php" method="POST" id="iosProfileConfig">
					<div class="modal-body">
						<div class="form-group">
							<label for="inputName">Display Name</label>
							<input type="text" name="displayName" class="form-control" id="displayName" aria-describedby="nameHelp" placeholder="From Name" required />
							<input type="hidden" name="process" value="1" />
							<small id="nameHelp" class="form-text text-muted">Please enter the name that will appear on outgoing mail.</small>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-dark" id="downloadIosConfig" value="Download Profile" />
					</div>
				</form>
			</div>
		</div>
	</div> <!-- end iOS Automatic Configuration Profile modal -->
	
	<!-- Script iOS config -->
	<script type="text/javascript">
		
		$( document ).ready(function() {
			$(function () {
				$('[data-toggle="popover"]').popover();
			});
		});
		
	</script>
	
<?php
include("include/footer.php");

function showLinkIfExists($link, $name, $img, $show_it)
{
	if (!$show_it) { return; }

	//I was considering adding checks for the presence of the http://host/$link through apache
	//but I think that will be too slow of an operation.
	//it's easier to just add/remove the options above.

	$prot="https://";

	$url = $prot.$_SERVER["HTTP_HOST"].$link;

	echo '
	<div class="col-md-4">
		<div class="card shadow-sm">
			<div class="card-header">
				<h4 class="my-0 font-weight-normal">'.$name.'</h4>
			</div>
			<div class="card-body">
				<a alt="'.$name.'" href="'.$url.'" target="_blank"><img class="mb-3" alt="'.$name.'" src="images/'.$img.'" border="0"></a>
				<a class="btn btn-lg btn-block btn-outline-dark" alt="'.$name.'" href="'.$url.'" target="_blank">Launch '.$name.'</a>
			</div>
		</div>
	</div>
	';

}

?>
