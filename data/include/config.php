<?php
//brand company name & url
$brandName = "Simplificare Hosting and Solutions, Inc.";
$brandUrl = "https://www.simplificare.net";	
$brandImage = "images/simplificare_logo_black.png";
	
//server info
$host="localhost";
$port="2222";
$ssl=true;

//webmail providers installed
$show_roundcube=true;
$show_squirrelmail=false;
$show_uebimiau=false;

?>
