<?php

if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
    $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $location);
    exit;
}

$version = "2.0.2";

header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");

session_name("setemail");
session_start();

include_once("include/config.php");
include_once("include/httpsocket.php");

if (!isset($_SESSION['ip']))
{
        $_SESSION['ip'] = $_SERVER["REMOTE_ADDR"];
}
else
if ($_SESSION['ip'] != $_SERVER["REMOTE_ADDR"])
{
	$_SESSION['ip'] = $_SERVER["REMOTE_ADDR"];
	$_SESSION['login'] = "";
	$_SESSION['password'] = "";

	showLogin("",$brandImage);
        exit(0);
}

if (isset($_POST['login']) && isset($_POST['password']))
{
	if (!is_email($_POST['login']))
	{
		showLogin("Please enter a valid email address",$brandImage);
		exit(0);
	}

	if (!is_pass($_POST['password']))
	{
		showLogin("Please enter a valid password",$brandImage);
		exit(0);
	}
		
        $_SESSION['login']    = $_POST['login'];
        $_SESSION['password'] = $_POST['password'];

		$email    = $_POST['login'];
        $pos      = strpos($email, "@");
        $domain   = substr($email, $pos+1);
        $user     = substr($email, 0, $pos);

        $pos                  = strpos($_SESSION['login'], '@');
        $_SESSION['domain']   = substr($_SESSION['login'], $pos+1);
        $_SESSION['user']     = substr($_SESSION['login'], 0, $pos);
				
        header("Location: ".$_SERVER["REQUEST_URI"]);
	exit(0); //this was added after... we'll do the login check on the next time round.
}

if (!isset($_SESSION['login']) || !is_email($_SESSION['login']) || !isset($_SESSION['password']) || !isset($_SESSION['domain']) || !isset($_SESSION['user']))
{
	showLogin("Please enter your login information",$brandImage);
        exit(0);
}

//if the data exists, they've already authenticated (we hope)
//but do this every time for index.php, cus it's good to see.
$names = explode("/", $_SERVER["SCRIPT_NAME"]);
$file = $names[count($names)-1];
if (!isset($_SESSION['inbox']) || !is_numeric($_SESSION['inbox']) || $file=="index.php")
{

	$sock = newSock();
	$sock->query('/CMD_API_EMAIL_ACCOUNT_QUOTA',
		array(
			'domain' => $_SESSION['domain'],
			'user' => $_SESSION['user'],
			'password' => $_SESSION['password'],
			'api' => '1',
			'quota' => 'yes'
		 ));

	$result = $sock->fetch_parsed_body();

	//this is for the no-header bug. Only needed for DA 1.31.1 and older.
	if (count($result) == 0)
	{
		parse_str($sock->fetch_result(), $result);
	}


	if ( $result['error'] != "0" )
	{
		showLogin("You have entered and invalid email or password<br>".$result['text'],$brandImage);
		exit(0);
	}

	//error=0&imap=20480&inbox=750&spam=0&total=21230&webmail=0
	$_SESSION['imap'] = $result['imap'];
	$_SESSION['inbox'] = $result['inbox'];
	$_SESSION['spam'] = $result['spam'];
	$_SESSION['total'] = $result['total'];
	$_SESSION['quota'] = $result['quota'];
	$_SESSION['webmail']=$result['webmail'];
}

function showLogin($message="",$image) {
?>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="DirectAdmin User Level Email Plugin Login Page">
	    <meta name="author" content="Simplificare Hosting and Solutions, Inc.">

		<title>E-Mail Account Login</title>

		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

		<link href="css/signin.css" rel="stylesheet">

	</head>
	<body class="text-center" onLoad="document.form.login.focus();">
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					<img class="mb-4" src="<?php echo $image; ?>" alt="" width="300">
					<?php if ($message!="") { echo '<div class="form-signin-message alert alert-primary" role="alert">'.$message.'</div>'; } ?>
					<form action="<?php echo $_SERVER['PHP_SELF']?>" method="POST" name="form" class="form-signin">

						<label for="inputEmail" class="sr-only">Email address</label>
						<input type="email" id="inputEmail" name="login" class="form-control" placeholder="Email address" required="" autofocus="">

						<label for="inputPassword" class="sr-only">Password</label>
						<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="">

						<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

						<p class="mt-5 mb-3 text-muted">&copy; 2009-2021</p>
					</form>
				</div>
			</div>
		</div>
</body></html>
<?php

}

function is_email($email)
{
        if (!preg_match('/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+(,\s?([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+)*$/', $email))
                return false;
        $len = strlen($email);
        if ($len<4 || $len > 60) return false;
        return true;
}

function is_pass($pass)
{
	return preg_match('/^([a-zA-Z0-9]|[~`!@#$%^&*(){}[\]<>_+-=])+$/', $pass);
}

function newSock()
{
	global $host;
	global $port;
	global $ssl;

	$tsock = new HTTPSocket;
	$tsock->set_method('POST');
	#$tsock->set_login('username','password');
	$tsock->set_ssl_setting_message('DirectAdmin appears to be using SSL. Change your include/config.php and set $ssl=true;');

	if ($ssl)
	{
		$tsock->connect("ssl://$host", $port);
	}
	else
	{
		$tsock->connect("$host", $port);
	}

	return $tsock;
}

function readable_byte($bytes)
{
	$i = floor(log($bytes) / log(1024));
    $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    return sprintf('%.02F', $bytes / pow(1024, $i)) * 1 . ' ' . $sizes[$i];
	//OLD
	// if ($bytes<1024)
	// 	return "$bytes Bytes";
	// if ($bytes<1024*1024)
	// 	return ($bytes/1024)." Kb";
	//
	// return ($bytes/(1024*1024))." Meg";
}

function no_go($msg)
{

        echo $msg;

        include("include/footer.php");
        exit(0);
}
