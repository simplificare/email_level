<?php
require_once("login.php");

include("include/header.php");

if (isset($_POST['action']) && $_POST['action'] == "change")
{
	//oldpassword
	//password1
	//password2

	if (!is_pass($_POST['oldpassword']))	{ no_go("Old password is not syntactically correct.<br/><br/><a href=\"change_pass.php\">Click here</a> to try again."); }
	if ($_POST['oldpassword'] != $_SESSION['password']) { no_go("Old password is not correct.<br/><br/><a href=\"change_pass.php\">Click here</a> to try again."); }
	if (!is_pass($_POST['password1']))     { no_go("New password is not syntactically correct.<br/><br/><a href=\"change_pass.php\">Click here</a> to try again."); }
	if (!is_pass($_POST['password2']))     { no_go("New password2 is not syntactically correct.<br/><br/><a href=\"change_pass.php\">Click here</a> to try again."); }
	if ($_POST['password1'] != $_POST['password2']) { no_go("New passwords do not match.<br/><br/><a href=\"change_pass.php\">Click here</a> to try again."); }

	$sock = newSock();
	$sock->query('/CMD_API_CHANGE_EMAIL_PASSWORD',
        array(
                'email' => $_SESSION['login'],
                'oldpassword' => $_POST['oldpassword'],
				'password1'   => $_POST['password1'],
				'password2'   => $_POST['password2'],
                'api'	      => '1',
         ));

	$result = $sock->fetch_parsed_body();

	if ( $result['error'] != "0" )
	{
        	no_go("Unable to change password:<br>".$result['text']);
	}

	
	echo "Password changed.<br><br><a href='index.php'>Click here</a> to return to the dashboard.";

	//since we don't want to get booted.
	$_SESSION['password'] = $_POST['password1'];


	include("include/footer.php");
	exit(0);
}

?>
<script type="text/javascript">
	/* Password strength indicator */
function passwordStrength(password) {
	var msg = ['Not Acceptable', 'Very weak', 'Weak', 'OK', 'Strong', 'Very Strong'];
	var desc = ['0%', '20%', '40%', '60%', '80%', '100%'];
	var descClass = ['', 'bg-danger', 'bg-danger', 'bg-warning', 'bg-success', 'bg-success'];
	var score = 0;

	// if password bigger than 6 give 1 point
	if (password.length > 6) score++;

	// if password has both lower and uppercase characters give 1 point	
	if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) score++;

	// if password has at least one number give 1 point
	if (password.match(/d+/)) score++;

	// if password has at least one special caracther give 1 point
	if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )	score++;

	// if password bigger than 12 give another 1 point
	if (password.length > 10) score++;
	
	// Display indicator graphic
	$("#pstrength").removeClass(descClass[score-1]).addClass(descClass[score]).css( "width", desc[score] );

	// Display indicator text
	$("#pstrength_text").html(msg[score]);

	// Output the score to the console log, can be removed when used live.
    //console.log(desc[score]);
}
</script>
<!--Change password Container-->
	<div class="container">
		<div class="row">
			<div class="col">
				<h4>Change Password</h4>
				<p>Enter your old password followed by the new password twice below.</p>
			</div> <!-- /col -->
		</div> <!-- /row -->
		
		<form action="?" method="POST">
			<input type="hidden" name="action" value="change" />
			<div class="form-group row">
				<label for="currentPassword" class="col-sm-2 col-form-label">Current Password: </label>
				<div class="col-sm-4">
					<input type="password" name="oldpassword" class="form-control" id="currentPassword" placeholder="Current password" />
				</div>
			</div>
			<div class="form-group row">
				<label for="newPassword1" class="col-sm-2 col-form-label">New Password:</label>
				<div class="col-sm-4">
					<input type="password" name="password1" minlength="8" class="form-control" id="newPassword1" placeholder="New password" />
				</div>
			</div>
			<div class="form-group row">
				<label for="newPasswordStrength" class="col-sm-2 col-form-label">Password Strength:</label>
				<div class="col-sm-4">
					<div class="progress">
						<div id="pstrength" class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
					</div>
					<div id="pstrength_text"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="newPassword2" class="col-sm-2 col-form-label">Re-type Password:</label>
				<div class="col-sm-4">
					<input type="password" name="password2" minlength="8" class="form-control" id="newPassword2" placeholder="Re-type password" />
				</div>
			</div>
			<input type="submit" class="btn btn-dark" value="Change Password" />
		</form>
	</div>
<!-- /Change password Container -->	

<script type="text/javascript">
	jQuery(document).ready(function(){
    	jQuery("#newPassword1").keyup(function() {
    	  passwordStrength(jQuery(this).val());
    	});
    });
</script>

<?php
include("include/footer.php");

?>
